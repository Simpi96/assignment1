export const Table = ()=>{
const person = 
    {
        "data": [
          {
            "name": "Lodha Experia",
            "wing": [
              {
                "name": "A",
                "rooms": []
              },
              {
                "name": "B",
                "rooms": [
                  { 
                    "flat_no": "303",
                    "name": "Alif",
                    "contact": 123,
                    "email": "a@a.com"
                  },
                  {
                    "flat_no": "301",
                    "name": "Sat",
                    "contact": 456,
                    "email": "b@a.com"
                  }
                ]
              },
              {
                "name": "C",
                "rooms": [
                  {
                    "flat_no": "401",
                    "name": "Alice",
                    "contact": 753,
                    "email": "r@a.com"
                  },
                  {
                    "flat_no": "102",
                    "name": "Kite",
                    "contact": 159,
                    "email": "d@a.com"
                  },
                  {
                    "flat_no": "501",
                    "name": "Anil",
                    "contact": 147,
                    "email": "vr@a.com"
                  }
                ]
              }
            ]
          },
          {
            "name": "Lodha Palava",
            "wing": [
              {
                "name": "A",
                "rooms": [
                  {
                    "flat_no": "102",
                    "name": "ABC",
                    "contact": 123,
                    "email": "q@a.com"
                  },
                  {
                    "flat_no": "302",
                    "name": "Suni",
                    "contact": 456,
                    "email": "b@a.com"
                  }
                ]
              },
              {
                "name": "B",
                "rooms": [
                  {
                    "flat_no": "303",
                    "name": "Alif",
                    "contact": 123,
                    "email": "a@a.com"
                  },
                  {
                    "flat_no": "301",
                    "name": "Sat",
                    "contact": 456,
                    "email": "b@a.com"
                  }
                ]
              },
              {
                "name": "C",
                "rooms": [
                  {
                    "flat_no": "401",
                    "name": "Axd",
                    "contact": 369,
                    "email": "df@a.com"
                  }
                ]
              }
            ]
          }
        ]
      }



    return(
     <table>
         <tr>
             <th>Building Name</th>
             <th>Wing No</th>
             <th>Room No</th>
             <th>Customer Name</th>
             <th>Contact No</th> 
             <th>Email Id</th>
         </tr>
         {
             person.data.map((_elmn)=>{
                 return (
                     _elmn.wing.map((_wing)=>{
                         return(_wing.rooms.map((_rooms)=>{
                            return(
                                <tr>
                                    <td>{_elmn.name}</td>
                                    <td>{_wing.name}</td>
                                    <td> {_rooms.flat_no} </td>
                                    <td> {_rooms.name} </td>
                                    <td> {_rooms.contact} </td>
                                    <td> {_rooms.email} </td>
                                </tr>
                            )
                         }))
                     })

                 )
             })
                 
             }
                 
             

             
         
     </table>
    )
}
